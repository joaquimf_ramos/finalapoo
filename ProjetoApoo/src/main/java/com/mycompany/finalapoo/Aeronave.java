/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.finalapoo;

/**
 *
 * @author jfvtr
 */

/**
 * Representa a super-classe Aeronave que vai extender caracteristicas para algumas sub-classes.
 * @author tomas
 */
public abstract class Aeronave {
    
    private String nRegisto;
    private String marca;
    private float vMax;
    private double autonomia;
    private float pMax;
    
    /**
     * Construtor do objecto Aeronave atribuindo-lhe todos os parâmetros.
     * @param nRegisto Representa número de registo de uma aeronave.
     * @param marca Representa a marca de uma aeronave.
     * @param vMax Representa a velocidade máxima de uma aeronave.
     * @param autonomia Representa a autonomia de uma aeronave.
     * @param pMax Representa o peso máximo de uma aeronave.
     */
    public Aeronave(String nRegisto, String marca, float vMax, double autonomia, float pMax) {
        this.nRegisto = nRegisto;
        this.marca = marca;
        this.vMax = vMax;
        this.autonomia = autonomia;
        this.pMax = pMax;
    }
    /**
     * Construtor vazio do objecto Aeronave para posterior atribuição de parâmetros de acordo com a necessidade.
     */
    public Aeronave() {
    }
   
    /**
     * Construtor do objecto Aeronave a partir de um array de Strings com os seus parâmetros transversais a todas as aeronaves deifinidos em posições fixas.
     * @param arr Array que contém toda a informação comum entre aeronaves.
     */
    public Aeronave(String[] arr) {
        
        this.nRegisto = arr[1];
        this.marca = arr[2];
        this.vMax = Float.parseFloat(arr[3]);
        this.autonomia = Double.parseDouble(arr[4]);
        this.pMax = Float.parseFloat(arr[5]);
        
    }
    /**
     * Função para escrever automaticamente todos os parâmetros de uma dada aeronave.
     */
    public void display(){
        System.out.println("Num de Registo: " + nRegisto + "\n" +" Marca: " +  marca + "\n" + "Velocidade Máxima: " +  vMax + "\n" + "Autonomia: " + autonomia + "\n" + "Peso Máximo: " + pMax + "\n" );
    }
    
    /**
     * Função para escrever automaticamente todos os parâmetros de uma dada aeronave em formato String.
     * @return Retorna toda a informação de uma aeronave em formate String.
     */
     public String displayString(){
        String display =(nRegisto + ";" +  marca + ";" + vMax + ";" + autonomia + ";" + pMax + ";" );
        return display;
    }
     
     /**
      * Função para retornar número de registo de uma aeronave.
      * @return Retorna o número de registo de uma aeronave.
      */
    public String getnRegisto() {
        return nRegisto;
    }
    /**
     * Função para definir número de registo de uma aeronave.
     * @param nRegisto Define o número de registo de uma aeronave.
     */
    public void setnRegisto(String nRegisto) {
        this.nRegisto = nRegisto;
    }
    /**
     * Função para retornar marca de uma aeronave.
     * @return Reorna a marca de uma aeronave.
     */
    public String getMarca() {
        return marca;
    }
    /**
     * Função para definir marca de uma aeronave.
     * @param marca Define a marca de uma aeronave.
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }
    /**
     * Função para retornar velocidade máxima de uma aeronave.
     * @return Retorna a velocidade máxima de uma aeronave.
     */
    public float getvMax() {
        return vMax;
    }
    /**
     * Função para definir velocidade máxima de um aeronave.
     * @param vMax Define a velocidade máxima de uma aeronave.
     */
    public void setvMax(float vMax) {
        this.vMax = vMax;
    }
    /**
     * Função para retornar autonomia de uma aeronave.
     * @return Retorna a autonomia de uma aeronave.
     */
    public double getAutonomia() {
        return autonomia;
    }
    /**
     * Função para definir autonomia de uma aeronave.
     * @param autonomia Define a autonomia de uma aeronave.
     */
    public void setAutonomia(double autonomia) {
        this.autonomia = autonomia;
    }
    /**
     * Função para retornar peso máximo de uma aeronave.
     * @return Retorna o peso máximo de uma aeronave.
     */
    public float getpMax() {
        return pMax;
    }
    /**
     * Função para definir peso máximo de uma aeronave.
     * @param pMax Define o peso máximo de uma aeronave. 
     */
    public void setpMax(float pMax) {
        this.pMax = pMax;
    }
    /**
     * Função para retornar número total de lugares de acordo com o tipo de aeronave.
     * @return Retorna o número total de lugares de uma aeronave.
     */
    public abstract int getNumLugares();
    /**
     * Função para retornar comprimento mínimo de pista.
     * @return Retorna o comprimento de pista de uma aeronave.
     */
    public abstract int getCompMinPista() ;
    /**
     * Função para retornar tipo de aeronave.
     * @return Retorna o tipo de aeronave.
     */
    public abstract String getTipoAero();   
    /**
     * Função para definir número de lugares de acordo com o tipo de aeronave.
     */
public abstract void setNumLugares();
}
