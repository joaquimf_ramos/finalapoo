/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.finalapoo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author jfvtr
 */

/**
 * Representa a classe que permite trabalhar directamente os ficheiros através do programa.
 * @author tomas
 */
public class FileEditor {

    /**
     * Método para ler de um dado ficheiro.
     * @param nomeFicheiro Representa o nome do ficheiro onde se encontra guardada toda a informação sobre aeronaves.
     * @return Retorna informação sobre aeronaves.
     */
    public static ArrayList<Aeronave> LerDoFicheiro(String nomeFicheiro) {
        ArrayList<Aeronave> aeronaves = new ArrayList<Aeronave>();

        try {
            File in = new File("C:\\Users\\jfvtr\\Desktop\\FicheirosAeronaves\\"+ nomeFicheiro);
            String[] arrLine;

            Scanner scan = new Scanner(in);

            //if (nomeFicheiro.isEmpty() == false) {
            while (scan.hasNextLine()) {

                arrLine = scan.nextLine().split(";");
                if (arrLine[0].equals("A")) {
                    aeronaves.add(new Aviao(arrLine));
                }
                else {
                    aeronaves.add(new Helicopter(arrLine));
                }
                //System.out.println(texto);
            }

            scan.close();
            return aeronaves;

            //}
        } catch (FileNotFoundException ex) {
            // ficheiro não existe ou está em uso
            System.out.println("Erro no FileNotFoundException");

        } catch (IOException ex) {
            // algum tipo de erro de leitura ou escrita
            System.out.println("Erro no IOException");
        } catch (ArrayIndexOutOfBoundsException ex) {
            // algum tipo de erro de leitura ou escrita
            System.out.println("Erro no NumberFormatException" + ex);
        } catch (NumberFormatException ex) {
            // algum tipo de erro de leitura ou escrita
            System.out.println("Erro no NumberFormatException" + ex);
        }

        return aeronaves;

    }
    /**
     * Método para escrever num dado ficheiro.
     * @param aeronaves Representa um ArrayList de aeronaves recebido.
     * @param nomeFicheiro Representa o nome do ficheiro a abrir.
     */
    public static void escreverNoFicheiro(ArrayList<Aeronave> aeronaves, String nomeFicheiro) {

        try {
            File ficheiro = new File("C:\\Users\\jfvtr\\Desktop\\FicheirosAeronaves\\" + nomeFicheiro);

            ficheiro.createNewFile();

            FileWriter writer = new FileWriter(ficheiro);

            BufferedWriter buffer = new BufferedWriter(writer);

            for (int i = 0; i < aeronaves.size(); i++) {
                if (aeronaves.get(i) != null) {
                    if (i != 0) {
                        buffer.newLine();
                    }
                    buffer.write(aeronaves.get(i).displayString());

                }
            }
            buffer.close();

            writer.close();

        } catch (FileNotFoundException ex) {
            // ficheiro não existe ou está em uso
            System.out.println("Erro no FileNotFoundException");

        } catch (IOException ex) {
            // algum tipo de erro de leitura ou escrita
            System.out.println("Erro no IOException");
            ex.printStackTrace();
        }
    }
}
