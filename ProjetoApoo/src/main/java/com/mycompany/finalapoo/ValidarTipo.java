/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.finalapoo;

import java.util.Scanner;

/**
 *
 * @author tomas
 */

/**
 * Representa a classe onde são implementados métodos para validações dos tipos de inputs do utilizador.
 * @author tomas
 */
public class ValidarTipo {
    /**
     * Método para validar input de utilizador do tipo integer.
     * @return Retorna valor de 'a' se válido.
     */
    public static int validInt() {
        Scanner ler = new Scanner(System.in);
        int a = -1;
        boolean entradaInvMenu;
        do {
            do {
                if (ler.hasNextInt()) {
                    a = ler.nextInt();
                    entradaInvMenu = true;

                } else {
                    entradaInvMenu = false;
                    System.out.println("ERRO: Entrada inválida!!");
                    ler.next();
                }
            } while (!(entradaInvMenu));
        } while (a <= 0);
        return a;
    }
    /**
     * Método para validar input do utilizador do tipo Float.
     * @return Retorna valor de 'a' se válido.
     */
    public static Float validFloat() {
        Scanner ler = new Scanner(System.in);
        Float a = -1f;
        boolean entradaInvMenu;
        do {
            do {
                if (ler.hasNextFloat()) {
                    a = ler.nextFloat();
                    entradaInvMenu = true;

                } else {
                    entradaInvMenu = false;
                    System.out.println("ERRO: Entrada inválida!!");
                    ler.next();
                }
            } while (!(entradaInvMenu));
        } while (a <= 0);
        return a;
    }
    /**
     * Método para validar input do utilizador do tipo Double.
    * @return Retorna valor de 'a' se válido. 
     */
    public static Double validDouble() {
        Scanner ler = new Scanner(System.in);
        Double a = -1d;
        boolean entradaInvMenu;
        do {
            do {
                if (ler.hasNextDouble()) {
                    a = ler.nextDouble();
                    entradaInvMenu = true;

                } else {
                    entradaInvMenu = false;
                    System.out.println("ERRO: Entrada inválida!!");
                    ler.next();
                }
            } while (!(entradaInvMenu));
        } while (a <= 0);
        return a;
    }
        /**
         * Método para validar input de menú.
        * @return Retorna valor de 'a' se válido. 
         */
        public static int validMenu() {
        Scanner ler = new Scanner(System.in);
        int a = -1;
        boolean entradaInvMenu;
        do {
            if (ler.hasNextInt()) {
                a = ler.nextInt();
                entradaInvMenu = true;

            } else {
                entradaInvMenu = false;
                System.out.println("ERRO: Entrada inválida!!");
                ler.next();
            }
        } while (!(entradaInvMenu));
        return a;
    }
    
}
