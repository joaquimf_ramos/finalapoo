/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.finalapoo;

/**
 *
 * @author jfvtr
 */

/**
 * Representa a class de motores.
 * @author tomas
 */
public class Motor {
    private String marca;
    private String tipo;
    /**
     * Construtor do objecto Motor com todos os seus parâmetros.
     * @param marca Representa a marca de um motor.
     * @param tipo Representa o tipo de um motor.
     */
    public Motor(String marca, String tipo) {
        this.marca = marca;
        this.tipo = tipo;
    }
    /**
     * Construtr do objecto Motor a partir de um array de Strings com os seus parâmetros.
     * @param arr String Array recebido da leitura do ficheiro.
     */
    public Motor(String[] arr) {
        
        this.marca = marca;
        this.tipo = tipo;
    }
    /**
     * Função para retornar a marca de um dado motor.
     * @return Retorna a marca de um motor.
     */
    public String getMarcaM() {
        return marca;
    }
    /**
     * Função para definir a marca de um dado motor.
     * @param marca Define a marca de um motor.
     */
    public void setMarcaM(String marca) {
        this.marca = marca;
    }
    /**
     * Função para retornar o tipo de um dado motor.
     * @return Retorna o tipo de um motor.
     */
    public String getTipoM() {
        return tipo;
    }
    /**
     * Função para definir o tipo de um dado motor.
     * @param tipo Define o tipo de um motor.
     */
    public void setTipoM(String tipo) {
        this.tipo = tipo;
    }
    

}
