/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.finalapoo;

import static com.mycompany.finalapoo.ValidarTipo.validDouble;
import static com.mycompany.finalapoo.ValidarTipo.validFloat;
import static com.mycompany.finalapoo.ValidarTipo.validInt;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author jfvtr
 */
/**
 * Respresenta a classe Helicoptero que herda caracteristicas da super-classe Aeronave.
 * @author tomas
 */
public class Helicopter extends Aeronave {
    private String TipoAero = "H";
    private float dRotor;
    private int nLugares;
    
   /**
    * Contrutor do objecto Helicoptero atribuindo-lhe todas os parâmetros.
     * @param nRegisto Representa número de registo de uma aeronave.
     * @param marca Representa a marca de uma aeronave.
     * @param vMax Representa a velocidade máxima de uma aeronave.
     * @param autonomia Representa a autonomia de uma aeronave.
     * @param pMax Representa o peso máximo de uma aeronave.
     * @param dRotor Representa o diâmetro do rotor de um helicoptero.
     * @param nLugares Representa o número de lugares de um helicoptero.
    */
    public Helicopter(String nRegisto, String marca, float vMax, double autonomia, float pMax,float dRotor, int nLugares) {
        super(nRegisto, marca, vMax, autonomia, pMax);
        this.dRotor = dRotor;
        this.nLugares = nLugares;
    }
    /**
     * Construtor vazio do objecto Helicoptero para posterior atribuição de parâmetros de acordo com as necessidades.
     */
    public Helicopter() {
    }
     /**
      * Método para escrever automaticamente todos os parâmetros de um Helicoptero.
      * @param arr String Array para a construção de um helicoptero.
      */
    public Helicopter(String[] arr) {
        super(arr);
        
        this.TipoAero = arr[0];
        this.dRotor = Float.parseFloat(arr[6]);
        this.nLugares = Integer.parseInt(arr[7]);
    }
    /**
     * Função para escrever automaticamente todos os parâmetros de um dado Avião.
     */
    @Override
    public void display(){
       
        System.out.println("Helicoptero " + "\n" + "Número de Registo: " + getnRegisto() + "\n" + "Marca: " + getMarca() + "\n" + "Velocidade Máxima: " + getvMax() + "\n" + "Autonomia: " + getAutonomia() + "\n" + "Peso Máximo: " + getpMax() +  "\n" + "Diametro do Rotor: " + dRotor + "\n" + "Número de Lugares: " + nLugares );
        
    }
    /**
     * Método para escrever automaticamente todos os parâmetros de um Helicoptero em formato String a partir de cast de todos os parâmetros transversais a todas as aeronaves +
     * todos os parâmetros únicos ao objecto Helicoptero.
     * @return Retorna toda a informação de um helicoptero em formate String. 
     */
    @Override
    public String displayString() {
      String display = (TipoAero + ";" + super.displayString() + dRotor + ";" + nLugares );
        
      return display;
    }

    /**
     * Função para retornar tipo de aeronave.
     * @return Retorna o tipo de aeronave.
     */
    @Override
    public String getTipoAero() {
        return TipoAero;
    }
    /**
     * Função para retornar diâmetro de rotor de um dado Helicoptero.
     * @return Retorna o diâmetro de rotor de um helicoptero.
     */
    public float getdRotor() {
        return dRotor;
    }
    /**
     * Função para definir o diâmetro de rotor de um dado Helicoptero.
     * @param dRotor DEfine o diâmetro de rotor de um helicoptero.
     */
    public void setdRotor(float dRotor) {
        this.dRotor = dRotor;
    }
    /**
     * Função para retornar o número de lugares de um dado Helicoptero.
     * @return Retorna o número de lugares de um helicoptero.
     */
    public int getnLugares() {
        return nLugares;
    }
    /**
     * Função para definir o número de lugares de um dado Helicoptero.
     * @param nLugares Deifne o número de lugares de um helicoptero.
     */
    public void setnLugares(int nLugares) {
        this.nLugares = nLugares;
    }
    /**
     * Função para retornar número de lugares de uma aeronave.
     * @return Retorna o número total de lugares de uma aeronave.
     */
    @Override
    public int getNumLugares() {
        return nLugares;
    }  
    /**
     * Função para retornar comprimento mínimo de pista de uma aeronave.
     * @return Retorna o comprimento mínimo de pista de uma aeronave.
     */
    @Override
    public int getCompMinPista() {
        return 999999;
    }
    
    /**
     * Método para criar um novo Helicoptero e adicioná-lo a uma ArrayList<Aeronave>.
     * @param aero Representa a ArraList de aeronaves recebida.
     * @return Adiciona um helicoptero à ArrayList<Aeronave>. 
     * 
     */
   
    public static Helicopter criarHeli(ArrayList<Aeronave> aero) {
//Falta validações
        Scanner ler = new Scanner(System.in);

        Helicopter a = new Helicopter();
        

        System.out.println("Insira o número de registo do Helicoptero.");
        String nRegisto = ler.next();
         for(Aeronave aeronave : aero){
            if(aeronave.getnRegisto().equals(nRegisto)){
                System.out.println("Já existe uma Aeronave com esse número de Registo. ");
                criarHeli(aero);
            }
        }
        a.setnRegisto(nRegisto);

        System.out.println("Insira a marca do avião.");
        String marcaA = ler.next();
        a.setMarca(marcaA);

        System.out.println("Insira a velocidade máxima do Helicoptero.");
        float vMax = validFloat();
        a.setvMax(vMax);

        System.out.println("Insira a autonomia do Helicoptero.");
        double autonomia = validDouble();
        a.setAutonomia(autonomia);

        System.out.println("Insira o peso máximo do Helicoptero.");
        float pMax = validFloat();
        a.setpMax(pMax);

        System.out.println("Insira o diâmetro do rotor do Helicoptero.");
        int dRotor = validInt();
        
        a.setdRotor(dRotor);

        System.out.println("Insira o número de lugares do Helicoptero.");
        int nLug = validInt();
        a.setnLugares(nLug);

        return a;

    }
    /**
     * Função para definir número total de lugares de um helicoptero.
     */
    @Override
    public void setNumLugares(){
           System.out.println("Insira o número de lugares do Helicoptero.");
        int nLug = validInt();
        
        this.setnLugares(nLug); 
    }

      
}
