/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.finalapoo;

import static com.mycompany.finalapoo.ValidarTipo.validDouble;
import static com.mycompany.finalapoo.ValidarTipo.validFloat;
import static com.mycompany.finalapoo.ValidarTipo.validInt;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author jfvtr
 */
/**
 * Respresenta a classe Avião que herda caracteristicas da super-classe Aeronave.
 * @author tomas
 */
public class Aviao extends Aeronave {

    private String TipoAero = "A";

    private ArrayList<Motor> motores;
    private int nTur;
    private int nExec;
    private int compMinPista;

    /**
     * Construtor vazia do objecto Avião para posterio atribuição de parâmetros de acordo com a necessidade.
     */
    public Aviao() {
    }
    /**
     * Construtor do objecto Avião a partir de um array de Strings com os seus parâmetros.
     * @param arr 
     * @param TipoAero Indica o tipo de aeronave.
     * @param nTur Representa o número de lugares turísticos de uma aeronave.
     * @param nExec Representa o número de lugares executivos de uma aeronave.
     * @param compMinPista Representa o comprimento mínimo de pista de uma aeronave.
     * @param motores Representa o conjunto de motores do avião.
     */
    public Aviao(String[] arr) {

        super(arr);
        
        this.TipoAero = arr[0];
        this.nTur = Integer.parseInt(arr[6]);
        this.nExec = Integer.parseInt(arr[7]);
        this.compMinPista = Integer.parseInt(arr[8]);

        this.motores = new ArrayList<Motor>();

        for (int i = 9; i <= arr.length - 1; i += 2) {
            motores.add(new Motor(arr[i], arr[i + 1]));
        }

    }
    /**
     * Construtor do objector Avião atribuindo-lhe todos os seus parâmetros.
     * @param nRegisto Representa número de registo de uma aeronave.
     * @param marca Representa a marca de uma aeronave.
     * @param vMax Representa a velocidade máxima de uma aeronave.
     * @param autonomia Representa a autonomia de uma aeronave.
     * @param pMax Representa o peso máximo de uma aeronave.
     * @param nTur Representa o número de lugares turísticos de uma aeronave.
     * @param nExec Representa o número de lugares executivos de uma aeronave.
     * @param compMinPista Representa o comprimento mínimo de pista de uma aeronave.
     * @param motores Representa o conjunto de motores do avião.
     */
    public Aviao(String nRegisto, String marca, float vMax, double autonomia, float pMax, int nTur, int nExec, int compMinPista, ArrayList<Motor> motores) {
        super(nRegisto, marca, vMax, autonomia, pMax);
        this.motores = motores;
        this.nTur = nTur;
        this.nExec = nExec;
        this.compMinPista = compMinPista;
    }
    /**
     * Função para retornar tipo de aeronave.
     * @return Retorna o tipo de aeronave.
     */
    @Override
    public String getTipoAero() {
        return TipoAero;
    }
    /**
     * Função para retornar motor(es) de uma dada aeronave.
     * @return Retorna o conjunto de motores do avião.
     */
    public ArrayList<Motor> getMotores() {
        return motores;
    }
    /**
     * Função para definir motor(es) de uma dada aeronave.
     * @param motores Define o conjunto de motores do avião.
     */
    public void setMotores(ArrayList<Motor> motores) {
        this.motores = motores;
    }
    /**
     * Função para retornar número de lugares turísticos de um avião.
     * @return Retorna o número de lugares turísticos do avião.
     */
    public int getnTur() {
        return nTur;
    }
    /**
     * Função para definir número de lugares turísticos de um avião.
     * @param nTur Define o número de lugares turísticos do avião.
     */
    public void setnTur(int nTur) {
        this.nTur = nTur;
    }
    /**
     * Função para retornar número de lugares executivos de um avião.
     * @return Retorna o número de lugares executivos do avião.
     */
    public int getnExec() {
        return nExec;
    }
    /**
     * Função para definir número de lugares executivos de um avião.
     * @param nExec Define o número de lugares executivos do avião.
     */
    public void setnExec(int nExec) {
        this.nExec = nExec;
    }
    /**
     * Função para retornar comprimento mínimo de pista de uma aeronave.
     * @return Retorna o comprimento mínimo de pista de um avião.
     */
    @Override
    public int getCompMinPista() {
        return compMinPista;
    }
    /**
     * Função para definir comprimento mínimo de pista de uma aeronave.
     * @param compMinPista Define o comprimento mínimo de pista de um avião.
     */
    public void setCompMinPista(int compMinPista) {
        this.compMinPista = compMinPista;
    }
    /**
     * Função para retornar número de lugares de uma aeronave.
     * @return Retorna o número total de lugares de uma aeronave.
     */
    @Override
    public int getNumLugares() {
        return (nTur + nExec);
    }
    /**
     * Função para escrever automaticamente todos os parâmetros de um dado Avião.
     */
    @Override
    public void display() {

        System.out.println("Avião" + "\n" +"Número de Registo: " + getnRegisto() + "\n" + "Marca: " + getMarca() + "\n" + "Velocidade Máxima: " + getvMax() + "\n" + "Autonomia: " + getAutonomia() + "\n" + "Peso Máximo: " + getpMax() + "\n" + "Número de Lugares Turistica: " + nTur + "\n" + "Número de Lugares Executiva: " + nExec + "\n" + "Comprimento de Pista: " +  compMinPista + "\n" + "Motores: " +  displayMotor());

    }
    /**
     * Função para escrever autoomaticamente todos os parâmetros de um Avião em formato String a partir de cast de todos os parâmetros transversais a todas as aeronaves 
     * + os parâmetros únicos ao objecto Avião.
     * @return Retorna toda a informação de um avião em formate String.
     */
    @Override
    public String displayString() {
        String display = (TipoAero + ";" + super.displayString() + nTur + ";" + nExec + ";" + compMinPista + toStringDisplayMotor(motores));

        return display;
    }

    /**
    *Método para escrever automaticamente todos os parâmetros de um dado motor. 
    * @return Retorna informação sobre os motores de um avião.
    */
    public String displayMotor() {

        
        String motorDisplay = "";
        for (Motor motor : this.motores) {
            motorDisplay += " | " + motor.getMarcaM() + " | " + motor.getTipoM();
            
        }
        return motorDisplay;
    }

    /**
     * Metodo @toStringDisplayMotor para converter o Array de motores em String.
     * @param motores Representa o conjunto de motores de um avião.
     * @return Retorna o conjunto de motores de um avião.
     */
    public String toStringDisplayMotor(ArrayList<Motor> motores) {

        String motorDisplay = "";
        for (Motor motor : motores) {
            motorDisplay += ";" + motor.getMarcaM() + ";" + motor.getTipoM();
        }
        return motorDisplay;
    }
    /**
     * Método para criar um novo Avião e adicioná-lo a uma ArrayList<Aeronave> .
     * @param aero Representa Arraylist de aeronaves.
     * @return Adiciona um avião ao ArrayList<Aeronave>
     */
    public static Aviao criarAviao(ArrayList<Aeronave> aero) {
//Falta validações
        Scanner ler = new Scanner(System.in);

        Aviao a = new Aviao();
        ArrayList<Motor> motores = new ArrayList<Motor>();

        System.out.println("Insira o número de registo do avião.");
        String nRegisto = ler.next();
        for (Aeronave aeronave : aero) {
            if (aeronave.getnRegisto().equals(nRegisto)) {
                System.out.println("Já existe uma Aeronave com esse númerode Registo. ");
                criarAviao(aero);
            }
        }
        a.setnRegisto(nRegisto);

        System.out.println("Insira a marca do avião.");
        String marcaA = ler.next();
        a.setMarca(marcaA);

        System.out.println("Insira a velocidade máxima do avião.");
        float vMax = validFloat();
        a.setvMax(vMax);

        System.out.println("Insira a autonomia do avião.");
        double autonomia = validDouble();
        a.setAutonomia(autonomia);

        System.out.println("Insira o peso máximo do avião.");
        float pMax = validFloat();
        a.setpMax(pMax);

        System.out.println("Insira o número de lugares turísticos.");
        int nTur = validInt();

        a.setnTur(nTur);

        System.out.println("Insira o número de lugares executivos.");
        int nExec = validInt();
        a.setnExec(nExec);

        System.out.println("Insira o comprimento mínimo de pista.");
        int CompMinPista = validInt();
        a.setCompMinPista(CompMinPista);

        System.out.println("Quantos motores existem neste avião?");
        int motorInput = validInt();

        for (int i = 0; i < motorInput; i++) {
            System.out.println("Insira a marca do motor " + i + ".");
            String marca = ler.next();

            System.out.println("Insira o tipo de motor " + i + ".");
            String tipo = ler.next();

            motores.add(new Motor(marca, tipo));

        }
        a.setMotores(motores);
        return a;

    }
    /**
     * Função para definir número total de lugares de uma aeronave.
     */
    @Override
    public void setNumLugares() {
        System.out.println("Insira o número de lugares turísticos.");
        int nTur = validInt();

        this.setnTur(nTur);

        System.out.println("Insira o número de lugares executivos.");
        int nExec = validInt();
        this.setnExec(nExec);
    }
}
