/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.finalapoo;

import static com.mycompany.finalapoo.FileEditor.LerDoFicheiro;
import static com.mycompany.finalapoo.ValidarTipo.validMenu;
import java.util.Scanner;
import java.util.ArrayList;

/**
 *
 * @author jfvtr
*/
public class Main {

    /**Inicializa o programa.
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String compAerea = "";
        ArrayList<Aeronave> aeronaves = new ArrayList<Aeronave>();

        
        System.out.println("LOGIN"
                + "\n---------------------------------------------------------------------"
                + "\nQual a sua companhia Aera: "
                + "\n[1] - TAP."
                + "\n[2] - RYANAIR."
                + "\n[3] - EASYJET."
                + "\n[4] - LUFTHANSA."
                + "\n---------------------------------------------------------------------"
                + "\n[0] - Sair.");
        int a = validMenu();

        //Inicia a validação da entrada do utilizador, restringindo o intervalo de entradas possiveis.
        //falta validar para remeter sempre para int!
        if (a > 4 || a < 0) {
            do {
                System.out.println("ERRO: Entrada inválida!!");
                System.out.println("");
                System.out.println("LOGIN"
                        + "\n---------------------------------------------------------------------"
                        + "\nQual a sua companhia Aera: "
                        + "\n[1] - TAP."
                        + "\n[2] - RYANAIR."
                        + "\n[3] - EASYJET."
                        + "\n[4] - LUFTHANSA."
                        + "\n---------------------------------------------------------------------"
                        + "\n[0] - Sair.");
                a = validMenu();
            } while (a > 4 || a < 0);
        }
        switch (a) {
            case 1:
                aeronaves = FileEditor.LerDoFicheiro("TAP.txt");
                compAerea = "TAP.txt";
                System.out.println("[TAP]");
                menu(aeronaves, compAerea);
            case 2:
                aeronaves = FileEditor.LerDoFicheiro("RYANAIR.txt");
                compAerea = "RYANAIR.txt";
                System.out.println("[RYANAIR]");
                menu(aeronaves, compAerea);
            case 3:
                aeronaves = FileEditor.LerDoFicheiro("EASYJET.txt");
                compAerea = "EASYJET.txt";
                System.out.println("[EASYJET]");
                menu(aeronaves, compAerea);
            case 4:
                aeronaves = FileEditor.LerDoFicheiro("LUFTHANSA.txt");
                compAerea = "LUFTHANSA.txt";
                System.out.println("[LUFTHANSA]");
                menu(aeronaves, compAerea);
            case 0:
                System.exit(0);
                break;

        }
    }

    /**
     * Inicia a função menu, pendindo uma entrada de um número inteiro ao
     * utilizador para selecionar uma opção.
     *
     * @param aero
     */
    public static void menu(ArrayList<Aeronave> aero, String cAerea) {

        System.out.println("MENU"
                + "\n---------------------------------------------------------------------"
                + "\nSelecione o que pretende fazer: "
                + "\n[1] - Adicionar nova Aeronave."
                + "\n[2] - Elminar Aeronave."
                + "\n[3] - Pesquisar Aeronave."
                + "\n[4] - Editar informações de Aeronave."
                + "\n---------------------------------------------------------------------"
                + "\n[0] - Sair.");
        int a = validMenu();

        //Inicia a validação da entrada do utilizador, restringindo o intervalo de entradas possiveis.
        //falta validar para remeter sempre para int!
        if (a > 4 && a < 0) {
            do {
                System.out.println("ERRO: Entrada inválida!!");
                System.out.println("");
                System.out.println("MENU"
                        + "\n---------------------------------------------------------------------"
                        + "\nSelecione o que pretende fazer: "
                        + "\n[1] - Adicionar nova Aeronave."
                        + "\n[2] - Elminar Aeronave."
                        + "\n[3] - Pesquisar Aeronave."
                        + "\n[4] - Editar informações de Aeronave."
                        + "\n---------------------------------------------------------------------"
                        + "\n[0] - Sair.");
                a = validMenu();
            } while (a > 4 || a < 0);
        }
        switch (a) {
            case 1:
                menuAddAero(aero, cAerea);
            case 2:
                removeAero(aero, cAerea);
            case 3:
                menuPesquisa(aero, cAerea);
            case 4:
                menuEdit(aero, cAerea);
            case 0:
                FileEditor.escreverNoFicheiro(aero, cAerea);
                System.exit(0);
        }
    }

    public static void removeAero(ArrayList<Aeronave> aero, String cAerea) {
        Scanner ler = new Scanner(System.in);
        boolean entradaInvalidaNumReg;
        boolean aeroFound = false;

        String input = "";
        int a = 0;
        int checker = 0;

        do {
            System.out.println("Insira o número de registo da aeronave a Remover:");
            if (ler.hasNext()) {
                input = ler.next();
                entradaInvalidaNumReg = true;
            } else {
                System.out.println("Entrada Inválida! Insira um registo de [A-Z;a-z].");
                entradaInvalidaNumReg = false;
                ler.next();
            }
        } while (!(entradaInvalidaNumReg));

        for (Aeronave aeronave : aero) {

            if (input.equals(aeronave.getnRegisto())) {

                aeroFound = true;
                System.out.println("Aeronave encontrada! "
                        + "\n");
                aeronave.display();
                System.out.println("Pertende remover a Aeronave? "
                        + "\n-------------------------------------------------------"
                        + "\n [1] - SIM."
                        + "\n [2] - RETROCEDER."
                        + "\n-------------------------------------------------------"
                        + "\n [3] - Menu Principal."
                        + "\n [0] - Sair.");
                do {
                    checker = validMenu();
                } while (checker > 3 || checker < 0);

                switch (checker) {
                    case 1:
                        //TODO
                        if (aeronave.getTipoAero().equals("A")) {
                            aero.remove(aeronave);
                            System.out.println("Avião removido com sucesso!"
                                    + "\n");
                        } else {
                            aero.remove(aeronave);
                            System.out.println("Helicoptero removido com sucesso!"
                                    + "\n");
                        }

                        menu(aero, cAerea);
                    case 2:
                        removeAero(aero, cAerea);
                    case 3:
                        menu(aero, cAerea);
                    case 0:
                        FileEditor.escreverNoFicheiro(aero, cAerea);
                        System.exit(0);
                }
            }
        }

        if (aeroFound != true) {

            System.out.println("");
            System.out.println("Não existem Aeronaves com esse Número de Registo. ");
            System.out.println("");
            System.out.println("\nPRIMA:"
                    + "\n--------------------------------------"
                    + "\n[1] - Para repetir Pesquisa de Aeronave a remover. "
                    + "\n[2] - Para voltar ao Menu."
                    + "\n--------------------------------------"
                    + "\n[0] - Sair.");
            do {
                a = validMenu();
            } while (a > 2 || a < 0);

            switch (a) {
                case 1:
                    removeAero(aero, cAerea);
                    break;
                case 2:
                    menu(aero, cAerea);
                    break;
                case 0:
                    FileEditor.escreverNoFicheiro(aero, cAerea);
                    System.exit(0);
            }
        }
    }

    public static void menuAddAero(ArrayList<Aeronave> aero, String cAerea) {
        System.out.println("ADICIONAR AERONAVE"
                + "\n-----------------------------------------------------------------------------------------------|"
                + "\nSelecione o que pretende Adicionar:"
                + "\n[1] - Adicionar Novo Avião."
                + "\n[2] - Adicionar Novo Helicoptero."
                + "\n----------------------------------------------------------------------------------------------|"
                + "\n[3] - Menu Principal."
                + "\n[0] - Sair.");

        int a = validMenu();
        //Inicia a validação da entrada do utilizador, restringindo o intervalo de entradas possiveis.
        //falta validar para remeter sempre para int!
        if (a > 3 || a < 0) {
            do {
                System.out.println("ERRO: Entrada inválida!!");
                System.out.println("");
                System.out.println("ADICIONAR AERONAVE"
                        + "\n-----------------------------------------------------------------------------------------------|"
                        + "\nSelecione o que pretende Adicionar:"
                        + "\n[1] - Adicionar Novo Avião."
                        + "\n[2] - Adicionar Novo Helicoptero."
                        + "\n----------------------------------------------------------------------------------------------|"
                        + "\n[3] - Menu Principal."
                        + "\n[0] - Sair.");

                a = validMenu();

            } while (a > 3 || a < 0);
        }

        switch (a) {
            case 1:
                Aviao nAviao = Aviao.criarAviao(aero);

                System.out.println("O seu Avião foi adicionado com sucesso."
                        + "\n");
                nAviao.display();
                System.out.println("");

                aero.add(nAviao);
                menuAddAero(aero, cAerea);
                break;
            case 2:
                Helicopter nHeli = Helicopter.criarHeli(aero);

                System.out.println("O seu Helicoptero foi adicionado com sucesso."
                        + "\n");
                nHeli.display();
                System.out.println("");
                aero.add(nHeli);
                menuAddAero(aero, cAerea);
                break;
            case 3:
                menu(aero, cAerea);
                break;
            case 0:
                FileEditor.escreverNoFicheiro(aero, cAerea);
                System.exit(0);
        }
    }

    public static void menuEdit(ArrayList<Aeronave> aero, String cAerea) {

        System.out.println("EDITAR"
                + "\n-----------------------------------------------------------------------------------------------|"
                + "\nSelecione o que pretende Editar:"
                + "\n[1] - Número de Lugares."
                + "\n-----------------------------------------------------------------------------------------------|"
                + "\n[2] - Menu Principal."
                + "\n[0] - Sair.");

        int a = validMenu();
        //Inicia a validação da entrada do utilizador, restringindo o intervalo de entradas possiveis.
        //falta validar para remeter sempre para int!
        if (a > 2 || a < 0) {
            do {
                System.out.println("ERRO: Entrada inválida!!");
                System.out.println("");
                System.out.println("EDITAR"
                        + "\n-----------------------------------------------------------------------------------------------|"
                        + "\nSelecione o que pretende Editar:"
                        + "\n[1] - Número de Lugares."
                        + "\n-----------------------------------------------------------------------------------------------|"
                        + "\n[2] - Menu Principal."
                        + "\n[0] - Sair.");

                a = validMenu();

            } while (a > 2 || a < 0);
        }

        switch (a) {
            case 1:
                editNumLug(aero, cAerea);
                break;
            case 2:
                menu(aero, cAerea);
            case 0:
                FileEditor.escreverNoFicheiro(aero, cAerea);
                System.exit(0);
        }
    }

    public static void editNumLug(ArrayList<Aeronave> aero, String cAerea) {

        Scanner ler = new Scanner(System.in);
        boolean entradaInvalidaNumReg;
        boolean aeroFound = false;

        String input = "";
        int a = 0;
        int checker = 0;

        do {
            System.out.println("Insira o número de registo da aeronave a Editar:");
            if (ler.hasNext()) {
                input = ler.next();
                entradaInvalidaNumReg = true;
            } else {
                System.out.println("Entrada Inválida! Insira um registo de [A-Z;a-z].");
                entradaInvalidaNumReg = false;
                ler.next();
            }
        } while (!(entradaInvalidaNumReg));

        for (Aeronave aeronave : aero) {

            if (input.equals(aeronave.getnRegisto())) {

                aeroFound = true;
                System.out.println("Aeronave encontrada! "
                        + "\n");
                aeronave.display();
                System.out.println("\n"
                        + "Com um total de número de lugares de " + (aeronave.getNumLugares()) + "."
                        + "\n");
                System.out.println("Pertende alterar número de lugares? "
                        + "\n-------------------------------------------------------"
                        + "\n [1] - SIM."
                        + "\n [2] - RETROCEDER."
                        + "\n-------------------------------------------------------"
                        + "\n [0] - SAIR.");
                do {
                    checker = validMenu();
                } while (checker > 2 || checker < 0);

                switch (checker) {
                    case 1:
                        aeronave.setNumLugares();
                        System.out.println("Aeronave alterada com sucesso!"
                                + "\n");
                        aeronave.display();
                        menuEdit(aero, cAerea);
                    case 2:
                        editNumLug(aero, cAerea);
                    case 0:
                        FileEditor.escreverNoFicheiro(aero, cAerea);
                        System.exit(0);

                }

               
            }
        }
        if (aeroFound != true) {

            System.out.println("");
            System.out.println("Não existem Aeronaves com esse Número de Registo. ");
            System.out.println("");
            System.out.println("\nPRIMA:"
                    + "\n--------------------------------------"
                    + "\n[1] - Para repetir Pesquisa de Aeronave a editar. "
                    + "\n[2] - Para voltar ao Menu de Pesquisa."
                    + "\n--------------------------------------"
                    + "\n[0] - Sair.");
            do {
                a = validMenu();
            } while (a > 2 || a < 0);

            switch (a) {
                case 1:
                    editNumLug(aero, cAerea);
                    break;
                case 2:
                    menuPesquisa(aero, cAerea);
                    break;
                case 0:
                    FileEditor.escreverNoFicheiro(aero, cAerea);
                    System.exit(0);
            }

        }
    }

    public static void menuPesquisa(ArrayList<Aeronave> aero, String cAerea) {

        System.out.println("PESQUISA"
                + "\n-----------------------------------------------------------------------------------------------|"
                + "\nSelecione como pretende Pesquisar:"
                + "\n[1] - Por Número de Registo."
                + "\n[2] - Por Intervalo de número de lugares."
                + "\n[3] - Por Comprimento de Pista."
                + "\n[4] - Por Autonomia Minima e Número Minimo de Lugares."
                + "\n----------------------------------------------------------------------------------------------|"
                + "\n[5] - Menu Principal."
                + "\n[0] - Sair.");

        int a = validMenu();
        //Inicia a validação da entrada do utilizador, restringindo o intervalo de entradas possiveis.
        
        if (a > 5 || a < 0) {
            do {
                System.out.println("ERRO: Entrada inválida!!");
                System.out.println("");
                System.out.println("PESQUISA"
                        + "\n-----------------------------------------------------------------------------------------------|"
                        + "\nSelecione como pretende Pesquisar:"
                        + "\n[1] - Por Número de Registo."
                        + "\n[2] - Por Intervalo de número de lugares."
                        + "\n[3] - Por Comprimento de Pista."
                        + "\n[4] - Por Autonomia Minima e Número Minimo de Lugares."
                        + "\n----------------------------------------------------------------------------------------------|"
                        + "\n[5] - Menu Principal."
                        + "\n[0] - Sair.");

                a = validMenu();

            } while (a > 5 || a < 0);
        }

        switch (a) {
            case 1:
                searchNumReg(aero, cAerea);
                break;
            case 2:
                searchNumLugar(aero, cAerea);
                break;

            case 3:
                searchCompPista(aero, cAerea);
                break;
            case 4:
                searchAutMinLugMin(aero, cAerea);
            case 5:
                menu(aero, cAerea);
            case 0:
                FileEditor.escreverNoFicheiro(aero, cAerea);
                System.exit(0);

        }
    }

    public static void searchNumReg(ArrayList<Aeronave> aero, String cAerea) {
        /**
         * Metodo de Pesquisa Número de Registo.
         */
        Scanner ler = new Scanner(System.in);
        boolean entradaInvalidaNumReg;
        boolean aeroFound = false;

        String input = "";
        int a = 0;

        do {
            System.out.println("Insira o número de registo a pesquisar:");
            if (ler.hasNext()) {
                input = ler.next();
                entradaInvalidaNumReg = true;
            } else {
                System.out.println("Entrada Inválida! Insira um registo de [A-Z;a-z].");
                entradaInvalidaNumReg = false;
                ler.next();
            }
        } while (!(entradaInvalidaNumReg));

        for (Aeronave aeronave : aero) {

            if (input.equals(aeronave.getnRegisto())) {

                aeroFound = true;
                System.out.println("Aeronave encontrada!"
                        + "\n");
                aeronave.display();
                System.out.println("\n");
                menuPesquisa(aero, cAerea);
                
                
            }
        }
        if (aeroFound != true) {

            System.out.println("");
            System.out.println("Não existem Aeronaves com esse Número de Registo. ");
            System.out.println("");
            System.out.println("\nPRIMA:"
                    + "\n--------------------------------------"
                    + "\n[1] - Para repetir tipo de Pesquisa. "
                    + "\n[2] - Para voltar ao Menu de Pesquisa."
                    + "\n--------------------------------------"
                    + "\n[0] - Sair.");
            do {
                a = validMenu();
            } while (a > 2 || a < 0);

            switch (a) {
                case 1:
                    searchNumReg(aero, cAerea);
                    break;
                case 2:
                    menuPesquisa(aero, cAerea);
                    break;
                case 0:
                    FileEditor.escreverNoFicheiro(aero, cAerea);
                    System.exit(0);
            }
            System.out.println("\nPRIMA:"
                    + "\n--------------------------------------"
                    + "\n[1] - Para repetir tipo de Pesquisa. "
                    + "\n[2] - Para voltar ao Menu de Pesquisa."
                    + "\n--------------------------------------"
                    + "\n[0] - Sair.");
            do {
                a = validMenu();
            } while (a > 2 || a < 0);

            switch (a) {
                case 1:
                    searchNumReg(aero, cAerea);
                    break;
                case 2:
                    menuPesquisa(aero, cAerea);
                    break;
                case 0:
                    FileEditor.escreverNoFicheiro(aero, cAerea);
                    System.exit(0);
            }
        }

    }

    public static void searchNumLugar(ArrayList<Aeronave> aero, String cAerea) {
        /**
         * Metodo de Pesquisa por intervalo de número de lugares.
         */
        Scanner ler = new Scanner(System.in);

        boolean entradaInvalida1;
        boolean entradaInvalida2;

        int a = 0;
        int b = 0;
        int max = 0;
        int min = 0;
        int countAero = 0;
        /**
         * Inicia a validação para o intervalo de pesquisa para o número de
         * lugares inserido pelo utilizador, apenas permite números Inteiros.
         */
        do {
            do {
                do {
                    System.out.println("Insira o primeiro número para o intervalo de lugares a pesquisar:");
                    if (ler.hasNextInt()) {
                        a = ler.nextInt();
                        entradaInvalida1 = true;
                    } else {
                        System.out.println("Entrada Inválida, insira um número inteiro.");
                        entradaInvalida1 = false;
                        ler.next();
                    }
                } while (!(entradaInvalida1));
                if (a <= 0) {
                    System.out.println("Entrada Inválida, insira um número inteiro positivo.");
                }
            } while (a <= 0);
            do {
                do {
                    System.out.println("Insira o segundo número para o intervalo de lugares a pesquisar:");
                    if (ler.hasNextInt()) {
                        b = ler.nextInt();
                        entradaInvalida2 = true;
                    } else {
                        System.out.println("Entrada Inválida, insira um número inteiro.");
                        entradaInvalida2 = false;
                        ler.next();
                    }
                } while (!(entradaInvalida2));
                if (b <= 0) {
                    System.out.println("Entrada Inválida, insira um número inteiro positivo.");
                }
            } while (b <= 0);
            if (a == b) {

                System.out.println("\n"
                        + "\nEntrada Inválida!! Os dois números são iguais."
                        + "\n");
            }
        } while (a == b);
        /**
         * Verifica qual dos dois valores submetidos a e b são o maior e o
         * menor.
         */
        if (a > b) {
            max = a;
            min = b;
        } else if (a < b) {
            max = b;
            min = a;
        }
        /**
         * Inicializa a iteração pelo ArrayList<Aeronave> de aeronaves
         * procurando pelo intervalo de pesquisa submetido pelo utilizador. Se
         * isso não se verificar, dá a possibilidade através de um menu, para
         * que o utilizador repita o processo ou retorne ao menu de Pesquisa.
         */
        for (Aeronave aeronave : aero) {

            if (aeronave.getNumLugares() <= max && aeronave.getNumLugares() >= min) {

                aeronave.display();
                countAero++;
            } else if (countAero == 0) {

                System.out.println("");
                System.out.println("Não existem Aeronaves com esse Intrevalo de Número de Lugares. ");
                System.out.println("");
                System.out.println("\nPRIMA:"
                        + "\n--------------------------------------"
                        + "\n[1] - Para repetir tipo de Pesquisa. "
                        + "\n[2] - Para voltar ao Menu de Pesquisa."
                        + "\n--------------------------------------"
                        + "\n[0] - Sair.");
                do {
                    a = validMenu();
                } while (a > 2 || a < 0);

                switch (a) {
                    case 1:
                        searchNumReg(aero, cAerea);
                        break;
                    case 2:
                        menuPesquisa(aero, cAerea);
                        break;
                    case 0:
                        FileEditor.escreverNoFicheiro(aero, cAerea);
                        System.exit(0);
                }
            }
        }
    }

    public static void searchCompPista(ArrayList<Aeronave> aero, String cAerea) {
        /**
         * Metodo de Pesquisa por Comprimento mínimo de Pista.
         */

        Scanner ler = new Scanner(System.in);
        boolean entradaInvalidaComp;

        int a = 0;
        int compMin = 99999;

        /**
         * Inicia a validação de pesquisa por Comprimento mínimo de pista
         * inserido pelo utilizador, apenas permite números Inteiros.
         */
        do {
            do {
                System.out.println("Insira o Comprimento de Pista pelo qual quer pesquisar: ");
                if (ler.hasNextInt()) {
                    a = ler.nextInt();
                    entradaInvalidaComp = true;
                } else {
                    System.out.println("Entrada Inválida, insira um número inteiro.");
                    entradaInvalidaComp = false;
                    ler.next();
                }
            } while (!(entradaInvalidaComp));
            if (a <= 0) {
                System.out.println("Entrada Inválida, insira um número inteiro positivo.");
            }
        } while (a <= 0);
        /**
         * Inicializa a iteração pelo ArrayList<Aeronave> de aeronaves
         * procurando pelo Comprimento Mínimo de Lugares. Se isso não se
         * verificar, dá a possibilidade através de um menu, para que o
         * utilizador repita o processo ou retorne ao menu de Pesquisa.
         */
        for (Aeronave aeronave : aero) {

            if (compMin > aeronave.getCompMinPista()) {
                compMin = aeronave.getCompMinPista();
            }
            if (aeronave.getCompMinPista() <= a) {

                aeronave.display();
            }
        }
        if (compMin > a) {
            System.out.println("");
            System.out.println("Não existem Aeronaves que possam aterrar nessa pista. ");
            System.out.println("");
            System.out.println("\nPRIMA:"
                    + "\n--------------------------------------"
                    + "\n[1] - Para repetir tipo de Pesquisa. "
                    + "\n[2] - Para voltar ao Menu de Pesquisa."
                    + "\n--------------------------------------"
                    + "\n[0] - Sair.");
            do {
                a = validMenu();
            } while (a > 2 || a < 0);

            switch (a) {
                case 1:
                    searchCompPista(aero, cAerea);
                    break;
                case 2:
                    menuPesquisa(aero, cAerea);
                    break;
                case 0:
                    FileEditor.escreverNoFicheiro(aero, cAerea);
                    System.exit(0);
            }
        }
    }

    public static void searchAutMinLugMin(ArrayList<Aeronave> aero, String cAerea) {
        /**
         * Metodo de Pesquisa por Autonomia Miníma e número mínimo de Lugares.
         */
        Scanner ler = new Scanner(System.in);

        boolean entradaInvalidaAut;
        boolean entradaInvalidaLug;

        int autMin = -1;
        int lugMin = -1;
        int a = -1;
        int countAero = 0;

        /**
         * Inicia as validações pesquisa de autonimia minima autMin do
         * utilizador, apenas permite números Inteiros.
         */
        do {
            do {
                System.out.println("Insira a autonomia mínima a pesquisar:");
                if (ler.hasNextInt()) {
                    autMin = ler.nextInt();
                    entradaInvalidaAut = true;
                } else {
                    System.out.println("Entrada Inválida, insira um número inteiro.");
                    entradaInvalidaAut = false;
                    ler.next();
                }
            } while (!(entradaInvalidaAut));
            if (autMin <= 0) {
                System.out.println("Entrada Inválida, insira um número inteiro positivo.");
            }
        } while (autMin <= 0);
        /**
         * Inicia as validações pesquisa por número minimo de lugares do
         * utilizador, apenas permite números Inteiros.
         *
         */
        do {
            do {
                System.out.println("Insira o número minímo de lugares a pesquisar:");
                if (ler.hasNextInt()) {
                    lugMin = ler.nextInt();
                    entradaInvalidaLug = true;
                } else {
                    System.out.println("Entrada Inválida, insira um número inteiro.");
                    entradaInvalidaLug = false;
                    ler.next();
                }

            } while (!(entradaInvalidaLug));
            if (autMin <= 0) {
                System.out.println("Entrada Inválida, insira um número inteiro positivo.");
            }
        } while (lugMin <= 0);
        /**
         * Inicializa a iteração pelo ArrayList<Aeronave> de aeronaves
         * procurando pelo numéro minimo de Lugares, ao mesmo tempo que procura
         * pela autonomia mínima de uma aeronave no mesmo array. Se isso não se
         * verificar, o contador countAero, permite saber se não foi encontrada
         * nenhuma aeronave e, nesse caso, dá a possibilidade através de um
         * menu, para que o utilizador repita o processo ou retorne ao menu de
         * Pesquisa.
         */
        for (Aeronave aeronave : aero) {
            if (lugMin <= aeronave.getNumLugares() && autMin <= aeronave.getAutonomia()) {
                aeronave.display();
                countAero++;
            } else if (countAero == 0) {

                System.out.println("");
                System.out.println("Não existem Aeronaves com essas características. ");
                System.out.println("");
                System.out.println("\nPRIMA:"
                        + "\n--------------------------------------"
                        + "\n[1] - Para repetir tipo de Pesquisa. "
                        + "\n[2] - Para voltar ao Menu de Pesquisa."
                        + "\n--------------------------------------"
                        + "\n[0] - Sair.");
                do {
                    a = validMenu();
                } while (a > 2 && a < 0);

                switch (a) {
                    case 1:
                        searchAutMinLugMin(aero, cAerea);
                        break;
                    case 2:
                        menuPesquisa(aero, cAerea);
                        break;
                    case 0:
                        FileEditor.escreverNoFicheiro(aero, cAerea);
                        System.exit(0);
                }
            }
        }
    }

}
